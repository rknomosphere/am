<?php

namespace Sncf\AccountManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Useram
 *
 * @ORM\Table(name="useram")
 * @ORM\Entity(repositoryClass="Sncf\AccountManagerBundle\Repository\UseramRepository")
 */
class Useram
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, unique=true)
     */
    private $token;


    /**
     * @var bool
     *
     * @ORM\Column(name="optin", type="boolean")
     */
    private $optin;

    /**
     * @var bool
     *
     * @ORM\Column(name="account", type="boolean")
     */
    private $account;

    /**
     * @var bool
     *
     * @ORM\Column(name="isValidate", type="boolean", nullable=true)
     */
    private $isValidate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Useram
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Useram
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Useram
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isValidate
     *
     * @param boolean $isValidate
     * @return Useram
     */
    public function setIsValidate($isValidate)
    {
        $this->isValidate = $isValidate;

        return $this;
    }

    /**
     * Get isValidate
     *
     * @return boolean 
     */
    public function getIsValidate()
    {
        return $this->isValidate;
    }

    /**
     * Set optin
     *
     * @param boolean $optin
     * @return Useram
     */
    public function setOptin($optin)
    {
        $this->optin = $optin;

        return $this;
    }

    /**
     * Get optin
     *
     * @return boolean 
     */
    public function getOptin()
    {
        return $this->optin;
    }

    /**
     * Set account
     *
     * @param boolean $account
     * @return Useram
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return boolean 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
