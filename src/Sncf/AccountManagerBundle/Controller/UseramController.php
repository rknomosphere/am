<?php
/**
 * Created by PhpStorm.
 * User: romeo
 * Date: 08/11/16
 * Time: 15:13
 */
namespace Sncf\AccountManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/user")
 */
class UseramController extends Controller {


    /**
     * @Route("/delete/{token}", name="delete_user_account")
     */
    public function deleteAction()
    {
        return $this->render('SncfAccountManagerBundle:Default:index.html.twig');
    }

    /**
     * @Route("/optin/{token}", name="delete_user_optin")
     */
    public function optinAction()
    {
        return $this->render('SncfAccountManagerBundle:Default:index.html.twig');
    }

    /**
     * @Route("/create/{email}", name="create_am_user")
     */
    public function createAction()
    {
        return $this->render('SncfAccountManagerBundle:Default:index.html.twig');
    }
    
    //  /users/exist/{email}
}