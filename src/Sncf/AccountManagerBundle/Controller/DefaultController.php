<?php

namespace Sncf\AccountManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sncf\AccountManagerBundle\Form\UseramType;
use Sncf\AccountManagerBundle\Entity\Useram;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $entity = new Useram();
        /*
        $form = $this->createForm(new UseramType(), $entity, array(
            'action' => $this->generateUrl('admin_management_cp_resource_update', array( 'id' => $entity->getId(), 'idCp' => $entity->getCaptivePortal()->getId() ) ),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Update')); */
        $form = $this->createForm(new UseramType(), $entity);
        
        return $this->render('SncfAccountManagerBundle:Default:index.html.twig',array('form'=>$form->createView()));
    }


}
